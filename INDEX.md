# Beye

Biew is a binary/hexadecimal viewer/editor. (requires i686+)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## BIEW.LSM

<table>
<tr><td>title</td><td>Beye</td></tr>
<tr><td>version</td><td>6.10a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-04-28</td></tr>
<tr><td>description</td><td>Biew is a binary/hexadecimal editor</td></tr>
<tr><td>keywords</td><td>Biew, binary, hexadecimal, viewer, editor</td></tr>
<tr><td>primary&nbsp;site</td><td>http://ibiblio.org/pub/micro/pc-stuff/freedos/files/distributions/1.2/repos/pkg-html/biew.html</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://sourceforge.net/projects/beye/files/biew/6.1.0/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License</td></tr>
<tr><td>summary</td><td>Biew is a binary/hexadecimal viewer/editor. (requires i686+)</td></tr>
</table>
